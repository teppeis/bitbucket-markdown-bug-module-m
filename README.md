A bug of Bitbucket's Markdown parser
====

Bitbucket's Markdown parser cannot parse a code block that includes string `module m =`.

* [A source page of Markdown file (reproduce.md)](https://bitbucket.org/teppeis/bitbucket-markdown-bug-module-m/src/ccb19b2b996ac92a27ab4e389f746a4382f47629/reproduce.md?at=master) causes an parse error. I cannot view the page.
* [A wiki page](https://bitbucket.org/teppeis/bitbucket-markdown-bug-module-m/wiki/Home) and [a issue page](https://bitbucket.org/teppeis/bitbucket-markdown-bug-module-m/issue/1/sample) are not parsed as Markdown. They are shown as plain text.